worker_processes 1;

user nobody nogroup;
pid /tmp/nginx.pid;
error_log /tmp/nginx.error.log;

events {
  worker_connections 1024;
  accept_mutex off; # set to 'on' if nginx worker_processes > 1
  multi_accept on;
  use epoll;
}

http {
  include mime.types;
  default_type application/octet-stream;
  access_log /tmp/nginx.access.log combined;
  sendfile on;
  keepalive_timeout 65;
  keepalive_requests 100000;
  tcp_nopush on;
  tcp_nodelay on;

  upstream django_server {
    server niamoto-django:8000 fail_timeout=0;
  }

  upstream flower_server {
    server niamoto-django:5555 fail_timeout=0;
  }

  server {
    listen 80 deferred;
    client_max_body_size 4G;

    # set the correct host(s) for your site
    server_name niamoto;

    # path for static files
    root /home/niamoto/niamoto-portal;

    location site_media/media/ {
        alias /home/niamoto/niamoto-portal/site_media/media/;
    }

    location /static/ {
        alias /home/niamoto/niamoto-portal/site_media/static/;
    }

    location / {
      # checks for static file, if not found proxy to app
      #rewrite ^/niamoto(.*) /$1 break;
      try_files $uri @proxy_to_app;
    }

    location @proxy_to_app {
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      # enable this if and only if you use HTTPS
      # proxy_set_header X-Forwarded-Proto https;
      proxy_set_header Host $http_host;
      # we don't want nginx trying to do something clever with
      # redirects, we set the Host: header above already.
      proxy_redirect off;
      proxy_pass http://django_server;
    }

    error_page 500 502 503 504 /500.html;
    location = /500.html {
      root /path/to/app/current/public;
    }
  }

  server {
    listen 5555 deferred;
    client_max_body_size 4G;

    # set the correct host(s) for your site
    server_name niamoto;

    location / {
      # checks for static file, if not found proxy to app
      #rewrite ^/flower(.*) /$1 break;
      try_files $uri @proxy_to_app;
    }

    location @proxy_to_app {
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      # enable this if and only if you use HTTPS
      # proxy_set_header X-Forwarded-Proto https;
      proxy_set_header Host $http_host;
      # we don't want nginx trying to do something clever with
      # redirects, we set the Host: header above already.
      proxy_redirect off;
      proxy_pass http://flower_server;
    }

    error_page 500 502 503 504 /500.html;
    location = /500.html {
      root /path/to/app/current/public;
    }
  }

}
